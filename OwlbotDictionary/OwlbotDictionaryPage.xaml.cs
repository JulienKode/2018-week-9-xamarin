﻿using System.Linq;
using Plugin.Connectivity;
using System.Net.Http;
using Xamarin.Forms;
using System;
using System.Diagnostics;

namespace OwlbotDictionary
{
    public partial class OwlbotDictionaryPage : ContentPage
    {
        public OwlbotDictionaryPage()
        {
            InitializeComponent();

            CheckInternetConnection();
            CrossConnectivity.Current.ConnectivityChanged += (sender, e) =>
            {
                CheckInternetConnection();
            };

        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            if (CheckInternetConnection() == true)
            {
                callInternet();
            }
        }

        void Handle_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
          
        }

        public async void callInternet() {
            HttpClient client = new HttpClient();

            var uri = new Uri(
                string.Format(
                    $"https://owlbot.info/api/v2/dictionary/" +
                    $"{((Entry)FirstText).Text.ToLower()}"));

            Debug.WriteLine(uri);

            var request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri;
            request.Headers.Add("Application", "application / json");

            HttpResponseMessage response = await client.SendAsync(request);
            Word[] words = null;
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Debug.WriteLine(content);
                words = Word.FromJson(content);
                Debug.WriteLine(words.Length);

                if (words.Length == 1) {
                    Debug.WriteLine(words[0].Definition);
                    DefinitionLabel.Text = words[0].Definition;
                    ExampleLabel.Text = words[0].Example;
                    TypeLabel.Text = words[0].Type;
                }
            }
        }

        public bool CheckInternetConnection()  {
            if (CrossConnectivity.Current.IsConnected == false)
            {
                DisplayAlert("Alert", "You are not Connected to Internet", "OK");
            }
            return CrossConnectivity.Current.IsConnected;
        }

    }
}
