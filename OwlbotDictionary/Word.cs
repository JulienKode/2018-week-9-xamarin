﻿using System;
namespace OwlbotDictionary
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class Word
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class Word
    {
        public static Word[] FromJson(string json) => JsonConvert.DeserializeObject<Word[]>(json, OwlbotDictionary.Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this Word[] self) => JsonConvert.SerializeObject(self, OwlbotDictionary.Converter.Settings);
    }

    internal class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
